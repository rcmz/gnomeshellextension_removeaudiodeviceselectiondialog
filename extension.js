const a = imports.ui.audioDeviceSelection;

let f;

function init() {}

function enable() {
	f = a.AudioDeviceSelectionDBus.prototype.OpenAsync;
	a.AudioDeviceSelectionDBus.prototype.OpenAsync = null;
}

function disable() {
	a.AudioDeviceSelectionDBus.prototype.OpenAsync = f;
	f = null;
}
